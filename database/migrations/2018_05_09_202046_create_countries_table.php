<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mcc', 5);
            $table->string('cc', 5);
            $table->string('name', 50);
            $table->float('pricePerSMS', 8, 6);
        });
		
		DB::table('countries')->insert([
			[ 'mcc' => '262', 'cc' => '49', 'name' => 'Germany', 'pricePerSMS' => 0.055],
			[ 'mcc' => '232', 'cc' => '43', 'name' => 'Austria', 'pricePerSMS' => 0.053],
			[ 'mcc' => '260', 'cc' => '48', 'name' => 'Poland', 'pricePerSMS' => 0.032]
		]);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
