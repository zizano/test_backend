<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('sms/send.{response_format}', 'SmsController@sendSms');
Route::get('countries.{response_format}', 'CountriesController@getCountries');
Route::get('sms/sent.{response_format}', 'SmsController@getSms');
Route::get('statistics.{response_format}', 'SmsController@getStatistics');