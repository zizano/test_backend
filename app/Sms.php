<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
   protected $fillable = ['from', 'to', 'mcc', 'price'];
   
   protected $hidden = ['id', 'updated_at'];
   
   const CREATED_AT = 'dateTime';
   
}
