<?php

namespace App\Repositories;

interface SmsRepositoryInterface
{
	public function create($request);
	
	public function find($id);
	
	public function get($input);
	
	public function getStatistics($input);
	
}
