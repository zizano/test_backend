<?php

namespace App\Repositories;

use App\Sms;
use Illuminate\Support\Facades\DB;

class SmsRepositoryEloquent implements SmsRepositoryInterface
{
    /**
    * @var Member
    */
   protected $model;
 
   public function __construct(Sms $model)
   {
      $this->model = $model ;  
   }
   /**
    * @return MemberInterface
    */
   public function find($id)
   {
      return $this->model->find($id);
   }
   
   public function create($request) {
	   	
		$country = checkCountryCode($request->to);		
		
		return $this->model->create([
			'from' => $request->from,
			'to' => $request->to,
			'mcc' => $country->mcc,
			'price' => $country->pricePerSMS,
//			'dateTime' => \Carbon::now()	
		]);
   }
   
   public function get($input) {
	   
		if (isset($input['dateTimeFrom'])) {
			$input['dateTimeFrom'] = str_replace('T', ' ', $input['dateTimeFrom']);
		}
		if (isset($input['dateTimeTo'])) {
			$input['dateTimeTo'] = str_replace('T', ' ', $input['dateTimeTo']);
		}
		
		$sms = $this->model;
		if (!empty($input['dateTimeFrom'])){
			$sms = $sms->where('dateTime', '>=', $input['dateTimeFrom']);
		}
		if (!empty($input['dateTimeTo'])){
			$sms = $sms->where('dateTime', '<=', $input['dateTimeTo']);
		}
		
		$data = [];
		
		$data['totalCount'] = $sms->count();
		
		$data['items'] = $sms->offset($input['skip'])->limit($input['take'])->get();
		
		return $data;
   }
   
	public function getStatistics($input) {
		
		$mcc_arr = isset($input['mccList']) ? explode(',', $input['mccList']) : [];
		
		$sms = $this->model->where(DB::raw('DATE(dateTime)'), '>=', $input['dateFrom'])->where(DB::raw('DATE(dateTime)'), '<=', $input['dateTo']);
		
		if (!empty($mcc_arr)) {
			$sms = $sms->whereIn('mcc', $mcc_arr);
		}
		
		return $sms->groupBy('day', 'mcc', 'price')->select(DB::raw('DATE(dateTime) as day'), 'mcc', 'price as pricePerSMS', DB::raw('count(*) as count, (count(*) * price) as totalPrice'))->get();
		
	}


}