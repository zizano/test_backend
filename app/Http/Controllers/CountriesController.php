<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Countries;

class CountriesController extends Controller
{	
    public function getCountries($response_format) {
		
		setResponseFormat($response_format);
		
		return response_formated(Countries::all());
		
	}

}
