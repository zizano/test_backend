<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Sms;
use App\Classes\SmsSenderInterface;
use App\Classes\ApiValidatorInterface;
use App\Repositories\SmsRepositoryInterface;

class SmsController extends Controller
{
	protected $sender;
	protected $validator;
	protected $sms_repo;

	public function __construct(SmsRepositoryInterface $sms_repo, SmsSenderInterface $sender, ApiValidatorInterface $validator) {
		
		$this->sender = $sender;
		$this->validator = $validator;
		$this->sms_repo = $sms_repo;
		
	}
	
    public function sendSms(Request $request, $response_format) {
		
		setResponseFormat($response_format);
		
		$input = $request->all();
		
		$this->validator->validate($input, [ 
			'from' => 'required|max:50',
			'to' => 'required|max:20',
			'text' => 'required|max:918',
		]);

		$sms = $this->sms_repo->create($request);
		
		if ($this->sender->sendSms($input)) {			
			$sms->state = 'Success';
			$sms->update();			
		}
		
		return response_formated(['state' => $sms->state]);
		
	}
	
    public function getSms(Request $request, $response_format) {
		
		setResponseFormat($response_format);
		
		$input = $request->all();
		
		$this->validator->validate($input, [ 
			'dateTimeFrom' => 'date_format:Y-m-d\TH:i:s',
			'dateTimeTo' => 'date_format:Y-m-d\TH:i:s',
			'skip' => 'integer|required|min:0',
			'take' => 'integer|required|min:0|max:1000',
		]);
		
		return response_formated(
			$this->sms_repo->get($input)
		);
		
	}
	
    public function getStatistics(Request $request, $response_format) {
		
		setResponseFormat($response_format);
		
		$input = $request->all();
		
		$this->validator->validate($input, [ 
			'dateFrom' => 'date_format:Y-m-d|required',
			'dateTo' => 'date_format:Y-m-d|required'
		]);
		
		return response_formated(
			$this->sms_repo->getStatistics($input)
		);
	}
}
