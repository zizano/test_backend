<?php namespace App\Classes;

interface ApiValidatorInterface {

    public function validate($request, $rules);

}
