<?php namespace App\Classes;

use Illuminate\Support\Facades\Log;

class DummySmsSender implements SmsSenderInterface {

    public function sendSms($input)
    {		
		$string = 'From: ' . $input['from'] . ', To: ' . $input['to'] . ', Text: ' . $input['text'];
		
		Log::info($string);
			
        return true;
    }
    
}
