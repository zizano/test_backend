<?php namespace App\Classes;

//use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ApiValidator implements ApiValidatorInterface {

    public function validate($input, $rules)
    {
		$validation = \Validator::make($input, $rules);
		
		$errors = $validation->errors();
		if (count($errors) > 0) {
			throw new \Exception($errors->first());
		}
		
        return true;
    }

    
}