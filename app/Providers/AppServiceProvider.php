<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Classes\SmsSenderInterface::class, \App\Classes\DummySmsSender::class);
        $this->app->bind(\App\Classes\ApiValidatorInterface::class, \App\Classes\ApiValidator::class);
        $this->app->bind(\App\Repositories\SmsRepositoryInterface::class, \App\Repositories\SmsRepositoryEloquent::class);
    }
}
