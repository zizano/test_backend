<?php

function setResponseFormat($response_format) {
	$response_format = strtolower($response_format);
	if (!in_array($response_format, ['json', 'xml'])) {
		throw new Exception('Wrong response format!');
	}
	Config::set('respnse_format', $response_format);
}

function getResponseFormat() {
	return Config::get('respnse_format') ?? null;
}

function response_formated($input) {
	$response_format = getResponseFormat();
	if ($response_format == 'xml') {
		return response()->xml($input);
	} else {
		return response()->json($input);
	}
}

function checkCountryCode($phone) {
	
	$phone = ltrim($phone, '+');
	
	$countries = \App\Countries::all();

	foreach($countries as $country) {
		if (stripos($phone, $country->cc) !== false) {
			return $country;
		}
	}
	
	throw new Exception('Country code not supported');

}


//Algorithms and logical thinking:

function getPatterns (string $start, string $stop, int $index = 0, $patterns = []) {
	
	$start_prefix = substr($start, 0, $index);
	$start_sub = substr($start, $index);
	
	$stop_prefix = substr($stop, 0, $index);
	$stop_sub = substr($stop, $index);
	
	if (intval($start_prefix) > intval($stop_prefix)) {
		return $patterns;
	}
	
	if (check_string_all_char($start_sub, '0')	&& (check_string_all_char($stop_sub, '9') || (intval($start_prefix) < intval($stop_prefix)))) {
		
		array_push($patterns, $start_prefix . '*');
		
		if ($index == 0) {			
			return $patterns;			
		} else {
			
			$last_digit = substr($start_prefix, -1);
			
			$start_prefix = str_pad((intval($start_prefix) + 1), $index, '0', STR_PAD_LEFT);
			
			if ($last_digit == '9') {
				$index--;
			}		
			return getPatterns ($start_prefix . $start_sub, $stop_prefix . $stop_sub, $index, $patterns);
		}
		
	} else {
		if ($start_sub != '') {
			$index++;
			return getPatterns ($start, $stop, $index, $patterns);
		} else {
			return $patterns;
		}
	}
	
}

function check_string_all_char(string $string, string $char) {
	
	$len = strlen($string);
	for ($i = 0; $i < $len; $i++) {
		if ($string[$i] != $char) {
			return false;
		}
	}
	return true;
	
}

//$res = getPatterns ('00000', '99999');
//$res = getPatterns ('10000', '29999');
//$res = getPatterns ('68000', '6899999999');
//$res = getPatterns ('0004000', '0008999');
//$res = getPatterns ('5570000', '5659999');
//$res = getPatterns ('3760000', '5259999');
//$res = getPatterns ('12345', '45678');
//$res = getPatterns ('000000', '399099');

//echo '<pre>';
//print_r($res);
